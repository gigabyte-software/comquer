<?php declare(strict_types=1);

namespace Comquer\Event;

use Comquer\DomainIntegration\Id;

class AggregateType extends Id implements \Comquer\DomainIntegration\Event\AggregateType
{
}
