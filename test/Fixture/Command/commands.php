<?php

return [
    \ComquerTest\Fixture\Command\DoSomething\DoSomething::class => \ComquerTest\Fixture\Command\DoSomething\DoSomethingHandler::class,
    \ComquerTest\Fixture\Command\DoSomethingElse\DoSomethingElse::class => \ComquerTest\Fixture\Command\DoSomethingElse\DoSomethingElseHandler::class,
];
