<?php

use ComquerTest\Fixture\Query\GetSomething\GetSomething;
use ComquerTest\Fixture\Query\GetSomething\GetSomethingHandler;

return [
    GetSomething::class => GetSomethingHandler::class,
];
